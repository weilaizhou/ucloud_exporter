package main

import (
	"flag"
	_ "net/http/pprof"
	"runtime"

	"gitee.com/mickeybee/ucloud_exporter/umonitor"
	"go.uber.org/zap"
)

var (
	// 命令行参数
	ucloudPrivateKey   = flag.String("ucloud.privatekey", "", "Ucloud api  PrivateKey.")
	ucloudPublicKey    = flag.String("ucloud.publickey", "", "Ucloud api  PublicKey.")
	ucloudTimeInterval = flag.Int64("ucloud.interval", 3600, "Ucloud time interval. ")
	logPath            = flag.String("log.path", "./ucloud_exporter.log", "log path")
	logLevel           = flag.String("log.level", "info", "'info','debug'")
	// 命令行参数
	listenAddr  = flag.Int("web.listen-port", 58086, "An port to listen on for web interface and telemetry.")
	metricsPath = flag.String("web.telemetry-path", "/metrics", "A path under which to expose metrics.")
	debug       = flag.Int("pprof.debug", 0, "pprof debug, socket default: 58086")
)

func main() {
	flag.Parse()
	logger := umonitor.InitLogger(*logPath, *logLevel)
	defer logger.Sync()
	defer func() {
		if err := recover(); err != nil {
			logger.Error("err", zap.Any("msg", err))
		}
	}()

	// DEBUG:
	if *debug > 0 {
		runtime.SetMutexProfileFraction(1) // 开启对锁调用的跟踪
		runtime.SetBlockProfileRate(1)     // 开启对阻塞操作的跟踪
	}
	umonitor.InitConf(
		*ucloudPrivateKey,
		*ucloudPublicKey,
		nil,
		logger,
	)

	go umonitor.ResourceHandle(ucloudTimeInterval)
	umonitor.PrometheusColletcor(*metricsPath, *listenAddr)
}
