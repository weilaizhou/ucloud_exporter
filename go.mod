module gitee.com/mickeybee/ucloud_exporter

go 1.15

require (
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/prometheus/client_golang v1.7.1
	github.com/ucloud/ucloud-sdk-go v0.17.4
	go.uber.org/zap v1.16.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
)
